from django.shortcuts import render, redirect
from django.contrib import messages

from user_service.models import Login
from user_service.model.UserModel import User

from user_service.email import send_email
from user_service.helpers import pagination, make_hash

from random import randint
from datetime import datetime



def home(request):
    if not request.user.is_authenticated:
        messages.warning(request, 'Please Login')
        return redirect('login')

    users = User.objects.all()
    user = User.objects.filter(id=request.user.id).first()
    users = pagination(users, request.GET.get('page'))
    return render(request, 'home.html', {'users':users, 'user':user})


def delete_my_account(request):
    if not request.user.is_authenticated:
        messages.warning(request, 'Please Login')
        return redirect('login')

    Login.objects.filter(id=request.user.id).delete()
    User.objects.filter(login_id=request.user.id).delete()
    messages.success(request, 'Your account has been removed successfully')
    return redirect('logout')


def add_new_user(request):
    if not request.user.is_authenticated:
        messages.warning(request, 'Please Login')
        return redirect('login')

    if request.method == 'POST':
        if 'profile_photo' in request.FILES:
            profile_photo = request.FILES['profile_photo']
        else:
            profile_photo = 'images/no_image1.png'

        data = {
            'profile_photo' : profile_photo,
            'first_name': request.POST['first_name'].title(),
            'last_name': request.POST['last_name'].title(),
            'gender': request.POST['gender'].title(),
            'email': request.POST['email'],
            'password': request.POST['password'],
            'confirm_password': request.POST['confirm_password'],
            'phone': request.POST['phone'],
        }

        if not data['first_name']:
            messages.error(request, 'FIRSTNAME_IS_REQUIRED')
            return render(request, 'register.html', {'form':data})

        if not data['gender']:
            messages.error(request, 'GENDER_IS_REQUIRED')
            return render(request, 'register.html', {'form':data})

        if not data['email']:
            messages.error(request, 'EMAIL_REQUIRED')
            return render(request, 'register.html', {'form':data})

        if not data['phone']:
            messages.error(request, 'PHONE_NUMBER_IS_REQUIRED')
            return render(request, 'register.html', {'form':data})

        check_phone = User.objects.filter(phone=data['phone']).first()
        if check_phone:
            messages.error(request, 'PHONE_NUMBER_ALREADY_EXISTS')
            return render(request, 'register.html', {'form': data})

        check_email = Login.objects.filter(username=data['email']).first()
        if check_email:
            messages.error(request, 'EMAIL_ALREADY_EXISTS')
            return render(request, 'register.html', {'form':data})

        if data['password'] != data['confirm_password']:
            messages.error(request, 'PASSWORD_MISMATCH, TRY_AGAIN')
            return render(request, 'register.html', {'form':data})

        new_login = Login(
            username = data['email'],
            password = make_hash(data['password']),
            active = True
        )
        new_login.save()

        User(
            login_id = new_login.id,
            first_name = data['first_name'],
            last_name = data['last_name'],
            gender = data['gender'],
            email = data['email'],
            phone = data['phone'],
            profile_photo = profile_photo,
        ).save()

        subject = 'Users - New Account'
        content = '''Dear, {} {}\n
                    Your Account successfully created.\n
                    Username : {}
                    password : {}\n
                    Thank You'''.format(
                        data['first_name'],
                        data['last_name'],
                        data['email'],
                        data['password'],
                    )
        send_email(data['email'], subject, content)
        messages.success(request, 'NEW_ACCOUNT_HAS_BEEN_CREATED_AND_OTP_SUCCESSFULLY')
        return redirect('home')
    return render(request, 'register.html')


def update_user(request):
    if not request.user.is_authenticated:
        messages.warning(request, 'Please Login')
        return redirect('login')

    User.objects.filter(login_id=request.POST['id']).update(
        first_name=request.POST['first_name'],
        last_name=request.POST['last_name'],
        phone=request.POST['phone'],
    )
    messages.success(request, 'Successfully Updated')
    return redirect('home')

def delete_user(request):
    if not request.user.is_authenticated:
        messages.warning(request, 'Please Login')
        return redirect('login')

    User.objects.filter(login_id=request.POST['id']).delete()
    Login.objects.filter(id=request.POST['id']).delete()
    messages.success(request, 'Successfully Deleted')

    if request.user.id == request.POST['id']:
        return redirect('logout')

    return redirect('home')
