from django.contrib.auth.models import auth
from django.shortcuts import render, redirect
from django.contrib import messages

from user_service.models import Login
from user_service.model.LoginHistoryModel import LoginHistory
from user_service.model.UserModel import User

from user_service.helpers import make_hash
from user_service.email import send_email

from random import randint
from datetime import datetime



def register(request):
    if request.user.is_authenticated:
            messages.info(request, 'You already registred')
            return redirect('home')

    if request.method == 'POST':
        if 'profile_photo' in request.FILES:
            profile_photo = request.FILES['profile_photo']
        else:
            profile_photo = 'images/no_image1.png'

        data = {
            'profile_photo' : profile_photo,
            'first_name': request.POST['first_name'].title(),
            'last_name': request.POST['last_name'].title(),
            'gender': request.POST['gender'].title(),
            'email': request.POST['email'],
            'password': request.POST['password'],
            'confirm_password': request.POST['confirm_password'],
            'phone': request.POST['phone'],
        }

        if not data['first_name']:
            messages.error(request, 'FIRSTNAME_IS_REQUIRED')
            return render(request, 'register.html', {'form':data})

        if not data['gender']:
            messages.error(request, 'GENDER_IS_REQUIRED')
            return render(request, 'register.html', {'form':data})

        if not data['email']:
            messages.error(request, 'EMAIL_REQUIRED')
            return render(request, 'register.html', {'form':data})

        if not data['phone']:
            messages.error(request, 'PHONE_NUMBER_IS_REQUIRED')
            return render(request, 'register.html', {'form':data})

        check_phone = User.objects.filter(phone=data['phone']).first()
        if check_phone:
            messages.error(request, 'PHONE_NUMBER_ALREADY_EXISTS')
            return render(request, 'register.html', {'form': data})

        check_email = Login.objects.filter(username=data['email']).first()
        if check_email:
            messages.error(request, 'EMAIL_ALREADY_EXISTS')
            return render(request, 'register.html', {'form':data})

        if data['password'] != data['confirm_password']:
            messages.error(request, 'PASSWORD_MISMATCH, TRY_AGAIN')
            return render(request, 'register.html', {'form':data})

        new_login = Login(
            username = data['email'],
            password = make_hash(data['password']),
        )
        new_login.save()

        User(
            login_id = new_login.id,
            first_name = data['first_name'],
            last_name = data['last_name'],
            gender = data['gender'],
            phone = data['phone'],
            email = data['email'],
            profile_photo = profile_photo,
        ).save()

        messages.success(request, 'Mr/Mrs/Miss {}, YOUR_ACCOUNT_HAS_BEEN_CREATED_AND_OTP_SUCCESSFULLY_SEND_TO_YOUR_MAIL'.format(data['first_name']))
        return generate_otp(data['first_name'], data['last_name'], data['email'])

    return render(request, 'register.html')


def generate_otp(first_name, last_name, email):
    randam_number = randint(1000, 9999)
    otp = str(randam_number) + ',' + email
    print(otp)
    subject = 'OTP - USER-SERVICE'
    content = '''Dear, {} {}\n
                Account successfully created.\n
                Enthe the following OTP to activate your account
                Your OTP : {}'''.format(first_name, last_name, randam_number)
    send_email(email, subject, content)
    response = redirect('otp')
    response.set_cookie('otp', otp)
    return response


def validate_otp(request):
    if request.method == 'POST':
        if 'otp' in request.COOKIES:
            otp = request.COOKIES['otp'].split(',')
            if request.POST['otp'] == otp[0]:
                login = Login.objects.filter(username=otp[1]).first()
                login.active = True
                login.save()
                messages.success(request, 'OTP VERIFIED, NOW YOU CAN LOGIN')
                response = redirect('login')
                response.delete_cookie('otp')
                return response

            messages.error(request, 'Invalid OTP')
            return redirect('otp')

        messages.success(request, 'OTP Not found')
        return redirect('register')

    return render(request, 'otp.html')


def resend_otp(request):
    if request.method == 'POST':
        login = Login.objects.filter(username=request.POST['email']).first()
        if not login:
            messages.error(request, 'Email not found')
            return redirect('login')

        otp = request.COOKIES['otp'].split(',')
        subject = 'OTP - USER-SERVICE'
        content = '''Dear, {} {}\n
                    Your OTP : {}'''.format(login.first_name, login.last_name, otp[0])
        send_email(login.username, subject, content)
        return redirect('otp')

    return render(request, 'resend_otp.html')


def login(request):
    if request.user.is_authenticated:
        return redirect('home')

    elif request.method == 'POST':
        check = Login.objects.filter(username=request.POST['username']).first()
        if not check:
            messages.error(request, 'Username or Email id not found')
            return render(request, 'login.html', {'username': request.POST['username']})

        user = User.objects.filter(login_id=check.id).first()
        if not check.active:
            messages.info(request, 'Your email ID in not verified. OTP is successfully send to your email id. To enter the OTP to activate your account')
            return generate_otp(user.first_name, user.last_name, check.username)

        login = auth.authenticate(username=request.POST['username'], password=request.POST['password'])
        if not login:
            messages.error(request, 'Sorry, Given password is wrong')
            return render(request, 'login.html', {'username':request.POST['username']})

        user = User.objects.filter(login_id=login.id).first()
        if not user:
            messages.error(request, 'Forbidden')
            return render(request, 'login.html', {'username':request.POST['username']})

        auth.login(request, login)

        new_login = LoginHistory(
            login_id=login.id,
            login_at=datetime.now(),
        )
        new_login.save()
        messages.success(request, 'Welcome {}'.format(user.first_name))
        return redirect('home')

    return render(request, 'login.html')


def logout(request):
    auth.logout(request)
    response = redirect('home')
    return response