from django.urls import path

from user_service.controllers.common_controller import (
    register,
    login,
    logout,
    validate_otp,
    resend_otp,
)
from user_service.controllers.user_controller import (
    home,
    delete_my_account,
    add_new_user,
    update_user,
    delete_user
)


urlpatterns = [
    path('register', register, name='register'),
    path('', login, name='login'),
    path('logout/', logout, name='logout'),
    path('validate-otp/', validate_otp, name='otp'),
    path('resend-otp/', resend_otp, name='resend_otp'),

    path('home', home, name='home'),
    path('delete/my-account', delete_my_account, name='delete_my_account'),
    path('add/user', add_new_user, name='add_new_user'),
    path('update/user', update_user, name='update_user'),
    path('delete/user', delete_user, name='delete_user'),
]