from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager


class UserManager(BaseUserManager):
    def create_user(self, username, password=None):
        if not username:
            raise ValueError('Email required')
        if not password:
            raise ValueError('Password required')
        user_obj = self.model(
            username = self.normalize_email(username)
        )
        user_obj.set_password(password)
        user_obj.save(using=self._db)
        return user_obj


class Login(AbstractBaseUser):

    username        =    models.CharField(max_length=255, null=False, unique=True)
    password        =    models.CharField(max_length=255, null=False)
    role            =    models.CharField(default='user', null=False, max_length=32)
    active          =    models.BooleanField(default=False)
    is_blocked      =    models.BooleanField(default=False)
    last_login      =    models.DateTimeField(null=True)
    created_at      =    models.DateTimeField(auto_now_add=True)
    updated_at      =    models.DateTimeField(auto_now=True)

    objects = UserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = []

    @property
    def is_active(self):
        return self.active

    class Meta:
        db_table = "login"
