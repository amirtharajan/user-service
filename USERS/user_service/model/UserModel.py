from django.db import models
from user_service.models import Login


class User(models.Model):

    login                       =    models.ForeignKey(Login, on_delete=models.CASCADE, unique=True, related_name='login_data')
    profile_photo               =    models.FileField(upload_to='images/profile_photos', default='images/no_image1.png')
    first_name                  =    models.CharField(max_length = 255, default='')
    last_name                   =    models.CharField(max_length = 255)
    gender                      =    models.CharField(max_length=32)
    phone                       =    models.CharField(max_length=32)
    email                       =    models.CharField(max_length=255)
    created_at                  =    models.DateTimeField(auto_now_add=True)
    updated_at                  =    models.DateTimeField(auto_now=True)

    objects = models.Manager()

    class Meta:
        db_table = "users"
